/* This program contains code from NetHack, and may be freely redistributed
 * under the terms of the NetHack General Public License. */
/* See LICENSE for details. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* define `isinteractive` such that we can use it on both windows & *nix
 * systems to check if stdin is an interactive tty or a pipe/redirect
 */
#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#include <unistd.h>
#define isinteractive isatty
#else
#include <io.h>
#define isinteractive _isatty
#endif
/* Set up simple boolean definitions */
typedef int boolean;
#define TRUE 1
#define FALSE 0
/* Buffer size */
#define BUFSZ 250
#define FDECL(f, p) f p

boolean FDECL(includes_arg, (int, char **, const char *));
char *FDECL(xcrypt, (const char *));
int FDECL(main, (int, char **));

/* Test whether an array of arguments contains a particular string */
boolean
includes_arg(argc, argv, target)
int argc;
char **argv;
const char *target;
{
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], target)) {
            return TRUE;
        }
    }
    return FALSE;
}

/* Trivial text encryption routine which can't be broken with `tr' */
/* Taken from NetHack (included in lib/makedefs.c and src/hacklib.c). */
char *
xcrypt(str)
const char *str;
{
    static char buf[BUFSZ];
    register const char *p;
    register char *q;
    register int bitmask;

    for (bitmask = 1, p = str, q = buf; *p; q++) {
        *q = *p++;
        if (*q & (32 | 64))
            *q ^= bitmask;
        if ((bitmask <<= 1) >= 32)
            bitmask = 1;
    }
    *q = '\0';
    return buf;
}

int
main(argc, argv)
int argc;
char **argv;
{
    char buf[BUFSZ];
    /* If the program was called without args, read from STDIN */
    if (argc < 2) {
        while (1) {
            /*   Show prompt only if STDIN is interactive; this allows us to
             *   treat piped input properly, as in a command like 
             *     % echo 'hello' | xc
             *   or 
             *     % xc <enciphered.txt */
            if (isinteractive(STDIN_FILENO)) { printf("? "); }
            /* If our stream of input is nonempty, pass it to xcrypt() */
            if (fgets(buf, BUFSZ, stdin) != NULL) {
                /* Remove trailing newline, if one exists */
                int pos = strcspn(buf, "\n");
                if (pos || buf[0] == '\n') {
                    buf[pos] = '\0';
                }
                char* result = xcrypt(buf);
                if (*result == '\0' && isinteractive(STDIN_FILENO)) {
                    /* If user has pressed enter on an empty line, print a
                       help message in case they are trying to quit */
                    printf("use ^C or ^D (EOF) to exit\n");
                } else {
                    /* Otherwise, print the result of the conversion */
                    printf("%s\n", result);
                }
            } else {
                /* Quit if buffer is empty (i.e. starts with EOF) */
                break;
            }
        }
    } else if (includes_arg(argc, argv, "--help") || includes_arg(argc, argv, "-h")) {
        printf("usage: %1$s [<text>]\n       echo <text> | %1$s\n       %1$s <<<\"<text>\"\n", argv[0]);
    } else {
        /* If arguments were included in the terminal, just convert each of
           the 'words' & print the resulting strings separated by spaces */
        int end = argc - 1;
        for (int i = 1; i < end; i++) {
            printf("%s ", xcrypt(argv[i]));
        }
        printf("%s\n", xcrypt(argv[end]));
    }
}

